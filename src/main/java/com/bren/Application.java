package com.bren;

import com.bren.view.ProgramView;

public class Application {
    public static void main(String[] args) {
        new ProgramView().show();
    }
}
