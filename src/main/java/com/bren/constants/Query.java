package com.bren.constants;

public interface Query {
    String SHOW_ALL_TABLES = "select tablename from pg_catalog.pg_tables where schemaname = 'public';";
    String SHOW_STRUCTURE_OF_ALL_TABLES = "select table_name, column_name, data_type, " +
            "is_nullable, is_identity from INFORMATION_SCHEMA.COLUMNS " +
            "where table_schema = 'public' order by table_name;";
    String QUERY_WITH_ALIAS_AND_MATH_FUNCTION = "select min(sale_price) as min_sale_price from contracts;";
    String QUERY_WITH_FILTER = "select brand, release_date from cars where release_date <> 2008;";
    String QUERY_WITH_OPERATOR_OR = "select clients.name, last_name, age " +
            "from clients " +
            "inner join cities city on clients.city_id = city.id " +
            "where city.name = 'Lviv' or city.name = 'Kyiv';";
    String QUERY_WITH_SORTING = "select name, last_name, phone, age from clients order by age;";
    String INSERT_INTO_CLIENTS = "insert into clients (name, last_name, city_id, age, phone, email)" +
            "values (?, ?, ?, ?, ?, ?);";
    String INSERT_INTO_CARS = "insert into cars (brand, model, photo, release_date, mileage)" +
            "values (?, ?, ?, ?, ?);";
    String UPDATE_DEALERS = "update dealers set name = ?, last_name = ?, photo = ?, city_id = ?, phone = ? " +
            "where id = ?;";
    String UPDATE_COUNTRIES = "update countries set name = ? where id = ?;";
}
