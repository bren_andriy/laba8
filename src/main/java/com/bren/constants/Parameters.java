package com.bren.constants;

public interface Parameters {
    String POSTGRESQL_DRIVER = "org.postgresql.Driver";
    String URL = "jdbc:postgresql://localhost:5432/commissioncarshop";
    String USER ="admin";
    String PASSWORD = "root";
    String MIN_SALE_PRICE = "min_sale_price";
    String BRAND = "brand";
    String RELEASE_DATE = "release_date";
    String NAME = "name";
    String LAST_NAME = "last_name";
    String AGE = "age";
    String PHONE = "phone";
    String TABLE_NAME = "table_name";
    String COLUMN_NAME = "column_name";
    String DATA_TYPE = "data_type";
    String IS_NULLABLE = "is_nullable";
    String TABLENAME = "tablename";

}
